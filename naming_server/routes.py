import aiohttp.web
from .views import *


def setup_routes(app: aiohttp.web.Application):
    app.router.add_get('/', choose_get_handler)
    app.router.add_post('/', choose_post_handler)
