import time
from typing import Iterable
import os
import json


class File:
    size: int
    checksum: str
    storage_servers: Iterable[str]
    path: str
    name: str
    mtime: int

    def __init__(self, path, name, storage_servers=None, size=0, root=None, save=True, mtime=None):
        self.size = size
        self.checksum = ''
        if storage_servers:
            self.storage_servers = storage_servers
        else:
            self.storage_servers = []
        self.path = path
        self.name = name
        if mtime:
            self.mtime = mtime
        else:
            self.mtime = time.time_ns()
        if root:
            self.root = root
        else:
            self.root = ''
        if save:
            self.save_info()

    def save_info(self):
        if self.path[0] == '/':
            path = self.path[1:]
        else:
            path = self.path
        path = os.path.join(self.root, path)
        print(f'Saving "{path}" file info.')
        file = open(path, 'w')
        file.write(self.get_info())
        file.close()

    def __str__(self):
        return self.name

    def get_info(self):
        return json.dumps({
            'size': self.size,
            'checksum': self.checksum,
            'servers': self.storage_servers,
            'path': self.path,
            'name': self.name,
            'mtime': self.mtime,
            'root': self.root
        })

    def update_mtime(self):
        self.mtime = time.time_ns()

    @staticmethod
    def from_file(path):
        with open(path, 'r') as f:
            info = json.loads(f.read())
            return File(info['path'], name=info['name'], size=info['size'],
                        storage_servers=info['servers'], save=False, root=info.get('root'),
                        mtime=info.get('mtime', 0))
