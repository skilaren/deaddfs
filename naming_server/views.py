import json
import pprint

from aiohttp import web
from http import HTTPStatus
from naming_server.utils import get_last_dir_dict, pick_storages, list_to_lined_string, JSONEncoderE, strip_path
from naming_server.models import File
import os
import requests

FS_INIT_HEADER = 'IN'
CREATE_FILE_HEADER = 'FN'
READ_FILE_HEADER = 'FR'
WRITE_FILE_HEADER = 'FW'
DELETE_FILE_HEADER = 'FD'
INFO_FILE_HEADER = 'FI'
COPY_FILE_HEADER = 'CP'
MOVE_FILE_HEADER = 'MV'
OPEN_DIRECTORY_HEADER = 'CD'
READ_DIRECTORY_HEADER = 'LS'
MAKE_DIRECTORY_HEADER = 'MD'
DELETE_DIRECTORY_HEADER = 'DD'
SUCCESS_WRITE_HEADER = 'WS'


async def choose_get_handler(request):
    params = request.rel_url.query
    operation = params['op']
    print(operation)
    if operation == FS_INIT_HEADER:
        return await init_fs(request)
    if operation == READ_FILE_HEADER or operation == WRITE_FILE_HEADER:
        return await find_file(request)
    if operation == DELETE_FILE_HEADER:
        return await delete_file(request)
    if operation == INFO_FILE_HEADER:
        return await file_info(request)
    if operation == READ_DIRECTORY_HEADER:
        return await list_files(request)
    if operation == MAKE_DIRECTORY_HEADER:
        return await make_directory(request)
    if operation == DELETE_DIRECTORY_HEADER:
        return await remove_directory(request)


async def choose_post_handler(request):
    params = await request.json()
    operation = params['op']
    print(operation)
    if operation == CREATE_FILE_HEADER:
        return await create_file(request)
    if operation == SUCCESS_WRITE_HEADER:
        return await save_file_info(request)


async def init_fs(request):
    params = request.rel_url.query
    path = params['path']
    reset = params['reset']
    # If file system was already created and there is no command to reset then return existing file system
    if len(request.app['fs']) > 0 and reset != 'True':
        print('File system already created')
        print(request.app['fs'])
        result = json.dumps(request.app['fs'], cls=JSONEncoderE)
        print(result)
        return web.Response(text=result, status=HTTPStatus.ALREADY_REPORTED)

    # Otherwise, initialize it
    root = request.app['root']
    path = strip_path(path)
    if not os.path.exists(os.path.join(root, path)):
        os.makedirs(os.path.join(root, path))

    path_split = path.split('/')
    current_directory = request.app['fs']
    for dir_name in path_split:
        current_directory[dir_name] = {}
        current_directory = current_directory[dir_name]
    if os.path.isdir(os.path.join(root, path)):
        print('File system initialized')
        print(request.app['fs'])
        return web.Response(status=HTTPStatus.OK)
    else:
        return web.Response(status=HTTPStatus.BAD_REQUEST)


async def create_file(request):
    params = await request.json()
    path = params['path']
    root = request.app['root']
    status, current_directory, file_name = get_last_dir_dict(request.app['fs'], path, create_file=True)
    if status:
        return web.Response(text=f'Can not create file: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    if current_directory.get(file_name):
        return web.Response(text=f'File "{file_name}" already exist.', status=HTTPStatus.NOT_MODIFIED)
    else:
        storage_servers = pick_storages(servers=request.app['storage_servers'],
                                        replication_factor=int(request.app['replication_factor']))
        current_directory[file_name] = File(path=path, name=file_name, storage_servers=storage_servers, root=root)
        return web.Response(status=HTTPStatus.CREATED)


async def find_file(request):
    params = request.rel_url.query
    path = params['path']
    status, current_directory, file_name = get_last_dir_dict(request.app['fs'], path)

    if status:
        return web.Response(text=f'Can not find file: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    file = current_directory.get(file_name)
    if not file:
        return web.Response(text=f'File "{file}" does not exist.', status=HTTPStatus.NOT_FOUND)
    else:
        response_text = list_to_lined_string(file.storage_servers)
        return web.Response(text=response_text, status=HTTPStatus.OK)


async def delete_file(request):
    params = request.rel_url.query
    path = params['path']
    root = request.app['root']
    status, current_directory, file_name = get_last_dir_dict(request.app['fs'], path)

    if status:
        return web.Response(text=f'Can not find file: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    file = current_directory.get(file_name)
    if file:
        current_directory.pop(file_name)
        if file.size > 0:
            payload = {
                'op': DELETE_FILE_HEADER,
                'path': path,
            }
            for server in file.storage_servers:
                requests.get(server, params=payload)
        path = strip_path(path)
        os.unlink(os.path.join(root, path))
        if not os.path.exists(os.path.join(root, path)):
            return web.Response(text=f'File "{file}" has been removed.', status=HTTPStatus.ACCEPTED)
        else:
            return web.Response(status=HTTPStatus.INTERNAL_SERVER_ERROR)
    else:
        return web.Response(status=HTTPStatus.NOT_FOUND)


async def file_info(request):
    params = request.rel_url.query
    path = params['path']
    status, current_directory, file_name = get_last_dir_dict(request.app['fs'], path)

    if status:
        return web.Response(text=f'Can not find file: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    file = current_directory.get(file_name)
    if file:
        response_text = file.get_info()
        pprint.pprint(response_text)
        return web.Response(text=response_text, status=HTTPStatus.OK)
    else:
        return web.Response(status=HTTPStatus.NOT_FOUND)


async def save_file_info(request):
    params = await request.json()
    path = params['path']
    size = params['size']
    servers = params['servers']
    print(servers)
    status, current_directory, file_name = get_last_dir_dict(request.app['fs'], path)
    if status:
        return web.Response(text=f'Can not find file: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    file = current_directory.get(file_name)

    file.size = int(size)
    file.storage_servers = servers
    file.update_mtime()
    file.save_info()
    return web.Response(status=HTTPStatus.OK)


async def list_files(request):
    params = request.rel_url.query
    path = params['path']
    status, current_directory, file_name = get_last_dir_dict(request.app['fs'], path)
    if status:
        return web.Response(text=f'Can not find directory within path: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    if file_name:
        return web.Response(text=f'Directory "{file_name}" is a file.',
                            status=HTTPStatus.NOT_ACCEPTABLE)
    files = [f_name for (f_name, x) in current_directory.items()]
    return web.Response(text=list_to_lined_string(files), status=HTTPStatus.OK)


async def make_directory(request):
    params = request.rel_url.query
    path = params['path']
    root = request.app['root']
    status, current_directory, dir_name = get_last_dir_dict(request.app['fs'], path, last_dir_none=True)
    if status:
        return web.Response(text=f'Can not find directory within path: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    path = strip_path(path)
    path = os.path.join(root, path)
    current_directory[dir_name] = {}
    os.mkdir(path)
    if os.path.exists(path):
        return web.Response(status=HTTPStatus.CREATED)
    else:
        return web.Response(status=HTTPStatus.INTERNAL_SERVER_ERROR)


async def remove_directory(request):
    params = request.rel_url.query
    path = params['path']
    root = request.app['root']
    status, current_directory, dir_name = get_last_dir_dict(request.app['fs'], path, separate_last_dir=True)
    if status:
        return web.Response(text=f'Can not find directory within path: there is no "{current_directory}" directory.',
                            status=HTTPStatus.BAD_REQUEST)
    path = strip_path(path)
    path = os.path.join(root, path)
    print(f'Deleting "{path}" directory')
    current_directory.pop(dir_name)
    os.rmdir(path)
    if not os.path.exists(path):
        return web.Response(status=HTTPStatus.ACCEPTED)
    else:
        return web.Response(status=HTTPStatus.NOT_MODIFIED)
