import os
import random
from json import JSONEncoder

from .models import File


class JSONEncoderE(JSONEncoder):
    def default(self, o):
        if isinstance(o, File):
            return 'file'
        else:
            return JSONEncoder.default(self, o)


def get_last_dir_dict(fs, path, last_dir_none=False, separate_last_dir=False, create_file=False):
    if path[-1] == '/':
        path = path[:-1]
    path_split = path.split('/')[1:]
    current_directory = fs
    for dir_name in path_split[:-1]:
        dir_dict = current_directory.get(dir_name)
        if isinstance(dir_dict, dict):
            current_directory = dir_dict
        else:
            # We can not found some directory within path
            return 1, dir_name, None
    if isinstance(current_directory.get(path_split[-1]), File):
        # The last element of all path is file
        return 0, current_directory, path_split[-1]
    elif isinstance(current_directory.get(path_split[-1]), dict):
        # The last element of all path is directory
        if separate_last_dir:
            # For rmdir operation we want to get parent directory and remove from it
            return 0, current_directory, path_split[-1]
        else:
            # For other operations we want to get last directory
            return 0, current_directory.get(path_split[-1]), None
    else:
        # Last directory in path does not exist
        if last_dir_none:
            # For mkdir operation we want last directory do not exist
            return 0, current_directory, path_split[-1]
        elif create_file:
            # For create file last entity should be not found (File we want to create)
            return 0, current_directory, path_split[-1]
        else:
            # For other operations we do not want it and hence we did not found given path
            return 1, current_directory, None


def pick_storages(servers, replication_factor):
    return random.sample(servers, k=replication_factor)


def list_to_lined_string(array):
    if len(array) == 0:
        return ''
    result = ''
    for e in array:
        result += str(e) + '\n'
    return result.strip()


def find_fs(root):
    root_len = len(root.split('/')[:-1])
    resulting_tree = {}
    current_dir = resulting_tree
    if os.path.exists(root):
        for root, dirs, files in os.walk(root):
            if resulting_tree:
                path_split = root.split('/')[root_len:]
                current_dir = resulting_tree
                for item in path_split:
                    current_dir = current_dir.get(item)

            for dir_name in dirs:
                current_dir[dir_name] = {}
            for file_name in files:
                file = File.from_file(os.path.join(root, file_name))
                current_dir[file_name] = file

    else:
        os.makedirs(root)
    return resulting_tree


def strip_path(path):
    result = path
    if result[-1] == '/':
        result = result[:-1]
    if result[0] == '/':
        result = result[1:]
    return result
