class Client:

    naming_server: str
    coding: str
    DEBUG: bool

    PORT: int

    FILE_CREATE_TIMEOUT: float

    FS_INIT_HEADER: str
    PING_HEADER: str
    CREATE_FILE_HEADER: str
    READ_FILE_HEADER: str
    WRITE_FILE_HEADER: str
    SUCCESS_WRITE_HEADER: str
    DELETE_FILE_HEADER: str
    INFO_FILE_HEADER: str
    COPY_FILE_HEADER: str
    MOVE_FILE_HEADER: str
    OPEN_DIRECTORY_HEADER: str
    READ_DIRECTORY_HEADER: str
    MAKE_DIRECTORY_HEADER: str
    DELETE_DIRECTORY_HEADER: str

    def __init__(self, server: str, root: str, reset: bool) -> None:
        self.naming_servers = server
        ...

    def __log__(self, message: str, force: bool = False) -> None: ...
    def get_storage_url(self, path: str, header: str) -> str: ...

    def file_create(self, filepath: str) -> int: ...
    def file_read(self, path: str, length: int, offset: int) -> bytes: ...
    def file_write(self,path: str, data: bytes, offset: int) -> int: ...
    def file_delete(self, path: str): ...
    def file_info(self, path: str) -> dict: ...
    def read_directory(self, path: str) -> list[str]: ...
    def make_directory(self, path: str, mode: int) -> None: ...
    def delete_directory(self, path: str) -> int: ...

    def init_from_naming_server(self, fs: dict): ...

    def create_structure(self, fs: dict, path: str): ...
