import os
import sys
import errno
from client.client import Client

from fuse import FUSE, FuseOSError, Operations


class FileSystem(Operations):
    def __init__(self, root, server, reset):
        self.root = root
        self.client = Client(server=server, root=root, reset=reset)

    # Helpers
    # =======

    def _full_path(self, partial):
        if partial.startswith('/'):
            partial = partial[1:]
        path = os.path.join(self.root, partial)
        return path

    @staticmethod
    def _readlink(fh):
        return os.readlink(f'/proc/self/fd/{fh}')

    # Filesystem methods
    # ==================

    def access(self, path, mode):
        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    # Not specified in requirements
    def chmod(self, path, mode):
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    # Not specified in requirements
    def chown(self, path, uid, gid):
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    # Not specified in requirements
    def getattr(self, path, fh=None):
        full_path = self._full_path(path)
        st = os.lstat(full_path)
        info = self.client.file_info(full_path)
        result = dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                                                          'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size',
                                                          'st_uid'))
        if isinstance(info, dict):
            result['st_size'] = info['size']
            result['st_mtime_ns'] = info['mtime']
            result['st_mtime'] = info['mtime'] / 10**9
        return result

    # Read directory ('ls' command)
    def readdir(self, path, fh):
        full_path = self._full_path(path)
        return self.client.read_directory(full_path)

    # Not specified in requirements
    def readlink(self, path):
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    # Not specified in requirements
    def mknod(self, path, mode, dev):
        return os.mknod(self._full_path(path), mode, dev)

    # Remove directory if it is empty
    def rmdir(self, path):
        full_path = self._full_path(path)
        res = self.client.delete_directory(full_path)
        if res == 0:
            os.rmdir(full_path)
        return res

    # Create directory
    def mkdir(self, path, mode):
        full_path = self._full_path(path)
        os.mkdir(full_path, mode)
        return self.client.make_directory(full_path, mode)

    # Not specified in requirements
    def statfs(self, path):
        full_path = self._full_path(path)
        stv = os.statvfs(full_path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
                                                         'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files',
                                                         'f_flag', 'f_frsize', 'f_namemax'))

    # Remove file ('rm' command)
    def unlink(self, path):
        full_path = self._full_path(path)
        os.unlink(full_path)
        return self.client.file_delete(full_path)

    def symlink(self, name, target):
        return os.symlink(name, self._full_path(target))

    def rename(self, old, new):
        return os.rename(self._full_path(old), self._full_path(new))

    def link(self, target, name):
        return os.link(self._full_path(target), self._full_path(name))

    def utimens(self, path, times=None):
        return os.utime(self._full_path(path), times)

    # File methods
    # ============
    # We creating and opening files as usual because OS will held process of creating
    # a file descriptor, but then for write and read we can get path of file
    # and then communicate with our naming server and storage servers

    def open(self, path, flags):
        # Open a file and create file descriptor
        full_path = self._full_path(path)
        return os.open(full_path, flags)

    def create(self, path, mode, fi=None):
        # Create a file and file descriptor
        full_path = self._full_path(path)
        self.client.file_create(full_path)
        return os.open(full_path, os.O_WRONLY | os.O_CREAT, mode)

    def read(self, path, length, offset, fh):
        # Read from file descriptor
        full_path = self._readlink(fh)
        return self.client.file_read(full_path, length, offset)

    def write(self, path, data, offset, fh):
        full_path = self._readlink(fh)
        return self.client.file_write(full_path, data, offset)

    def truncate(self, path, length, fh=None):
        full_path = self._full_path(path)
        with open(full_path, 'r+') as f:
            f.truncate(length)

    def flush(self, path, fh):
        return os.fsync(fh)

    def release(self, path, fh):
        return os.close(fh)

    def fsync(self, path, fdatasync, fh):
        return self.flush(path, fh)


def main(mount_point, remote_mount_point, server, reset=False):
    FUSE(FileSystem(remote_mount_point, server, reset), mount_point, nothreads=True, foreground=True, nonempty=True)

