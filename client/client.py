import json
import os

import requests
from http import HTTPStatus
import errno


class Client:
    coding = 'ascii'
    DEBUG = True

    PORT = 32323

    FILE_CREATE_TIMEOUT = 10

    FS_INIT_HEADER = 'IN'
    CREATE_FILE_HEADER = 'FN'
    READ_FILE_HEADER = 'FR'
    WRITE_FILE_HEADER = 'FW'
    SUCCESS_WRITE_HEADER = 'WS'
    DELETE_FILE_HEADER = 'FD'
    INFO_FILE_HEADER = 'FI'
    COPY_FILE_HEADER = 'CP'
    MOVE_FILE_HEADER = 'MV'
    OPEN_DIRECTORY_HEADER = 'CD'
    READ_DIRECTORY_HEADER = 'LS'
    MAKE_DIRECTORY_HEADER = 'MD'
    DELETE_DIRECTORY_HEADER = 'DD'

    def __init__(self, server, root, reset):
        self.naming_server = server
        response = requests.get(self.naming_server, params={
            'op': self.FS_INIT_HEADER,
            'path': root,
            'reset': reset,
        })
        if response.status_code == HTTPStatus.OK:
            self.__log__(f'Client initialized with naming server: {self.naming_server}')
        elif response.status_code == HTTPStatus.ALREADY_REPORTED:
            self.__log__(f'Found file system on naming server:\n'
                         f'{response.text}')
            fs = json.loads(response.text)
            self.init_from_naming_server(fs)
        else:
            self.__log__(f'Can not initialize file system on naming server.\n'
                         f'Status code: {response.status_code}')

    def __log__(self, message, force=False):
        if self.DEBUG or force:
            print(message)

    def get_storage_url(self, path, header):
        payload = {
            'op': header,
            'path': path,
        }
        response = requests.get(self.naming_server, params=payload)
        servers = []
        if response.status_code == HTTPStatus.OK:
            for server in response.text.split('\n'):
                servers.append(server)
        return servers

    def file_create(self, filepath):
        payload = {
            'op': self.CREATE_FILE_HEADER,
            'path': filepath,
        }
        response = requests.post(self.naming_server, json=payload)
        if response.status_code == HTTPStatus.CREATED:
            self.__log__(f'File "{filepath}" created')
            return 0
        else:
            self.__log__(f'Error during creating {filepath}.\n'
                         f'Status code: {response.status_code}')
            return 1

    def file_read(self, path, length, offset):
        # Get the storage server URL for reading
        storage_servers = self.get_storage_url(path, self.READ_FILE_HEADER)

        # Send request to storage server for reading
        payload = {
            'op': self.READ_FILE_HEADER,
            'path': path,
            'len': length,
            'offset': offset,
        }
        mtime = os.stat(path).st_mtime_ns
        for server in storage_servers:
            try:
                response = requests.get(server, params=payload)
                if response.status_code == HTTPStatus.OK:
                    if mtime < int(response.headers['mtime']):
                        return response.content
                    else:
                        print(f'File "{path}" on server "{server}" are out of date')
            except requests.exceptions.ConnectionError:
                print(f'Can not establish connection with server "{server}"')
        return 0

    def file_write(self, path, data, offset):
        # Get the storage server URL for reading
        storage_servers = self.get_storage_url(path, self.WRITE_FILE_HEADER)

        # Send request to storage server for writing
        payload = {
            'op': self.WRITE_FILE_HEADER,
            'mode': 0,
            'path': path,
            'data': data.decode(encoding='utf-8'),
            'offset': offset,
        }

        # Write file on each given replica and keep track of replicas which were written successfully
        bytes_written = 0
        success_servers = []
        for server in storage_servers:
            try:
                response = requests.get(server, params=payload)
                if response.status_code == HTTPStatus.ACCEPTED:
                    print(f'Data were written on server: "{server}". Bytes written: {int(response.text)}')
                    success_servers.append(server)
                    bytes_written = int(response.text)
                else:
                    print(f'Error during writing in file on server "{server}" occur.\n'
                          f'Status code: {response.status_code}')
            except requests.exceptions.ConnectionError:
                print(f'Can not establish connection with server "{server}"')

        # If no server succeed then return error
        if len(success_servers) == 0:
            return -1
        else:
            # Send file info about successfully written file to the naming server
            payload = {
                'op': self.SUCCESS_WRITE_HEADER,
                'path': path,
                'servers': success_servers,
                'size': len(data)
            }
            print(success_servers)
            response = requests.post(self.naming_server, json=payload)
            if response.status_code == HTTPStatus.OK:
                return bytes_written
            else:
                return -1

    def file_info(self, path):
        payload = {
            'op': self.INFO_FILE_HEADER,
            'path': path,
        }
        response = requests.get(self.naming_server, params=payload)
        if response.status_code == HTTPStatus.OK:
            return json.loads(response.text)
        else:
            return 0

    def file_delete(self, path):
        # Send request to naming server to delete file
        payload = {
            'op': self.DELETE_FILE_HEADER,
            'path': path,
        }
        response = requests.get(self.naming_server, params=payload)
        if response.status_code == HTTPStatus.ACCEPTED:
            self.__log__(f'File {path} removed.')
            return 0
        else:
            self.__log__(f'File {path} can not be removed.\n'
                         f'Status code: {response.status_code}')
            return 1

    def read_directory(self, path):
        # Just request the content from naming server
        payload = {
            'op': self.READ_DIRECTORY_HEADER,
            'path': path,
        }
        response = requests.get(self.naming_server, params=payload)
        directory_content = []
        if len(response.text) == 0:
            self.__log__(f'Directory content is empty')
            return directory_content
        else:
            for entity in response.text.split('\n'):
                directory_content.append(entity)
            self.__log__(f'Directory content is {directory_content}')
            return directory_content

    def make_directory(self, path, mode):
        # Add directory to the current path on naming server
        payload = {
            'op': self.MAKE_DIRECTORY_HEADER,
            'path': path,
        }
        response = requests.get(self.naming_server, params=payload)
        if response.status_code == HTTPStatus.CREATED:
            print(f'Directory {path} created')
            return None
        else:
            print(f'Error creating "{path}" directory\n'
                  f'Status code: {response.status_code}')
            return 1

    def delete_directory(self, path):
        payload = {
            'op': self.DELETE_DIRECTORY_HEADER,
            'path': path,
        }
        response = requests.get(self.naming_server, params=payload)
        if response.status_code == HTTPStatus.ACCEPTED:
            print(f'Directory "{path}" removed')
            return 0
        else:
            print(f'Error during directory removing\n'
                  f'Status code: {response.status_code}')
            return -1

    def init_from_naming_server(self, fs):
        self.create_structure(fs, '/')

    def create_structure(self, fs, path):
        for key, value in fs.items():
            if isinstance(value, dict):
                dir_path = path + key + '/'
                if not os.path.exists(dir_path):
                    os.mkdir(dir_path)
                self.create_structure(value, dir_path)
            if isinstance(value, str):
                file_path = path + key
                if not os.path.exists(file_path):
                    os.open(file_path, os.O_CREAT)
