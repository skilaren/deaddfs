import aiohttp.web
from .views import *


def setup_routes(app: aiohttp.web.Application):
    app.router.add_get('/', choose_handler)
