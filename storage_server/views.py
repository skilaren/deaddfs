from aiohttp import web
from http import HTTPStatus
import os
import errno


CREATE_FILE_HEADER = 'FN'
READ_FILE_HEADER = 'FR'
WRITE_FILE_HEADER = 'FW'
DELETE_FILE_HEADER = 'FD'
INFO_FILE_HEADER = 'FI'
COPY_FILE_HEADER = 'CP'
MOVE_FILE_HEADER = 'MV'
OPEN_DIRECTORY_HEADER = 'CD'
READ_DIRECTORY_HEADER = 'LS'
MAKE_DIRECTORY_HEADER = 'MD'
DELETE_DIRECTORY_HEADER = 'DD'


async def choose_handler(request):
    params = request.rel_url.query
    operation = params['op']
    print(operation)
    if operation == READ_FILE_HEADER:
        return await read_file(request)
    if operation == DELETE_FILE_HEADER:
        return await delete_file(request)
    if operation == WRITE_FILE_HEADER:
        return await write_file(request)


async def write_file(request):
    params = request.rel_url.query
    mode = params['mode']
    path = params['path']
    data = params['data'].encode(encoding='utf-8')
    offset = int(params['offset'])
    if not os.path.exists(path):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                raise
    with open(path, 'wb+') as file:
        file.seek(offset)
        file.write(data)
    return web.Response(text=str(len(data)), status=HTTPStatus.ACCEPTED)


async def read_file(request):
    params = request.rel_url.query
    path = params['path']
    length = int(params['len'])
    offset = int(params['offset'])
    print(path)
    extension = path.split('.')[-1]
    if extension == 'swp':
        try:
            os.makedirs(os.path.dirname(path))
            os.open(file=path, flags=os.O_CREAT)
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                raise
    if not os.path.exists(path):
        return web.Response(status=HTTPStatus.NOT_FOUND)
    mtime = str(os.stat(path).st_mtime_ns)
    with open(path, 'rb') as file:
        file.seek(offset)
        response_content = file.read(length)
    return web.Response(body=response_content, content_type='application/octet-stream', status=HTTPStatus.OK,
                        headers={'mtime': mtime})


async def delete_file(request):
    params = request.rel_url.query
    path = params['path']
    os.remove(path)
    return web.Response(text=f'File "{path}" has been removed.', status=HTTPStatus.OK)
