Distributed File System project written on **Python** (not for production use)

#### Documentation:
    
https://deaddfs.readthedocs.io/en/latest/

#### Prerequisites

    Python >= 3.7
    PIP for your Python env
    
**Attention: it is recommended to use isolated Python environment to run project in 
order to avoid dependencies conflicts** 

And also you need to run 

    mkdir /var/lib/deadfs/storage/
    
   to give FUSE mount point for remote FS

#### Quick start

There are several parts of this system: Client, Naming Server and Storage Server.

To run **Client** clone this repository and run:

    python client-start.py /path/to/mount/your/fs namingserveraddr reset-option
    
    For example:
    
    python client-start.py /home/username/workfs/ http://0.0.0.0:8080
    
    Reset option is either True or False and are optional. If you choose reset then all existing structure
    on naming server will be removed and reinitialized.
    
To run **Naming server** you can either do it from cloning git repository or with running Docker container:

    docker run --name naming_server -e STORAGE_SERVERS=35.176.54.27,18.130.89.206 -p 8080:8080/tcp -t -i skilaren/skilaren-imgs:naming_server

    OPTIONS:
    STORAGE_SERVERS - reachable from naming server ips of storage servers
    R_FACTOR - replication factor
    ROOT - directory which will contain structure of DFS
 
 To run **Storage server** you can either do it from cloning git repository or with running Docker container
  as well as Naming server:
  
    docker run --name storage1 8080:8080/tcp -t -i skilaren/skilaren-imgs:storage_server
