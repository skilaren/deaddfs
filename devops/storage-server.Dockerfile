FROM python:3.8-alpine3.10

RUN apk update && apk upgrade && apk add bash
RUN apk add gcc linux-headers musl-dev libffi-dev openssl-dev g++
COPY . ./app

RUN pip install --no-cache -r ./app/requirements.txt
RUN mkdir -p /var/lib/deadfs/storage
EXPOSE 8080/tcp
ENTRYPOINT ["python", "./app/storage_server.py"]
