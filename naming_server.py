from aiohttp import web
from naming_server.routes import setup_routes
from naming_server.utils import find_fs
import sys
import configparser
import pprint
import os

if __name__ == '__main__':
    if len(sys.argv) == 1:
        root = os.environ.get('ROOT')
        replication_factor = os.environ.get('R_FACTOR')
        servers = os.environ.get('STORAGE_SERVERS')
        storage_servers = []
        for server in servers.split(','):
            storage_servers.append(f'http://{server.strip()}:8080')
    else:
        conf_file = sys.argv[1]
        config = configparser.ConfigParser()
        config.read(conf_file)
        root = config['DEFAULT']['root']
        replication_factor = int(config['DEFAULT']['r_factor'])
        servers = config['DEFAULT']['servers']
        storage_servers = []
        for server in servers.split(','):
            storage_servers.append(f'http://{server.strip()}:8080')
    app = web.Application()
    app['fs'] = find_fs(root)
    app['root'] = root
    app['replication_factor'] = replication_factor
    app['storage_servers'] = storage_servers

    print(f'Initialized fs is: ')
    pprint.pprint(app['fs'])
    print(f'Storage servers are: {storage_servers}')
    print(f'FS root is: {root}')

    setup_routes(app)
    web.run_app(app)
