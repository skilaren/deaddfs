.. DeadDFS documentation master file, created by
   sphinx-quickstart on Sat Nov 23 20:59:39 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DeadDFS's documentation!
===================================

.. toctree::
   :maxdepth: 2

   arch/arch

This is a project of a Distributed File System written in **Python** using HTTP with **aiohttp** library and Unix FUSE
interface for custom file systems



Content
==================

* :doc:`Architecture <arch/arch>`
* :ref:`modindex`
* :ref:`search`
