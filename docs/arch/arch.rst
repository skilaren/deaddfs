
Architecture
===================================

Client talks to the Naming Server with HTTP requests which have such query parameters:

|    {
|        'op': two-symbol header of operation
|        'path': path of file or directory
|        ... other operation specific data
|    }

On response Client receives correspondent data like: bytes written, storage servers for file etc.

Storage server keeps all DFS structure both in-memory within running program and in server's Linux file system.
For example if Client sends File Create message then Storage Server program creates new File object and create file
with info about original file in server's file system.