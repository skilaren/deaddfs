from client.client_cli import main
import sys
import os

if __name__ == '__main__':
    remote_mount = '/var/lib/deadfs/storage/'
    if not os.path.exists(remote_mount):
        try:
            os.makedirs(remote_mount)
        except OSError:
            print(f'No permission to write to {remote_mount}'
                  f'Run this with superuser or create it manually')
    if len(sys.argv) > 3:
        main(sys.argv[1], remote_mount, sys.argv[2], sys.argv[3])
    else:
        main(sys.argv[1], remote_mount, sys.argv[2])
